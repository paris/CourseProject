Feature: Reporting
Description
Actors: Customer and DTU Pay or Merchant and DTU Pay

  Scenario: Customer requests an activity report
    Given A registered customer
    When The customer requests an activity report to DTU Pay
    Then DTUPay answers with a list of transactions in a format like (date,merchant,amount and barcode used)

  Scenario: Merchant requests an activity report
    Given A registered merchant
    When The merchant requests an activity report to DTU Pay
    Then DTU Pay answers with a list of transactions in a format like (date,amount and barcode used)