Feature Payments on DTU Pay
Description:
Actors: Customer, Merchant and DTU Pay

  Scenario: Customer makes a successful payment
    Given A registered customer
    When The customer has valid barcodes available
    And the merchant consumes the customer barcode
    Then DTU Pay makes a request to the bank to make the transaction
    And the bank answers a successful status to DTU Pay
    And DTU Pay registers this transaction
    And DTU Pay notifies the customer and the merchant than the transaction has been successful
    And the transaction token is marked as used on the client

  Scenario: Customer makes a failed payment
    Given A registered customer
    When The customer does not have any barcode available
    And the customer tries to display a barcode
    Then The customer receives a error message indicating that he needs to request for new tokens