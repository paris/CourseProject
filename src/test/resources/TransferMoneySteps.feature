Feature: Transfer Money
Description
Actors DTU Pay and Bank API

  Scenario: A successful transfer request
    Given A request to the bank API
    When DTU Pay requests the bank to perform a transaction
    And the transaction is possible
    Then The bank answers with a success
    And DTUPay notifies customer and merchant the success status

  Scenario: A failed transfer request
    Given A request to the bank API
    When  DTU Pay request the bank to perform a transaction
    And the transaction is not possible
    Then The bank answers answers with a failure
    And DTUPay notifies customer and merchant the error status