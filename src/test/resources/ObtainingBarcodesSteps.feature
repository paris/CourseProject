Feature Obtaining barcode
Description
Actors Customer, DTU Pay

  Scenario A registered customer is allowed to request n barcodes
    Given A registered customer
    And the customer is allowed to request a barcode
    When The customer requests a set of barcodes
    Then The customer receives a set of tuples (barcode ID,barcode URL)

  Scenario A registered customer is not allowed to request more barcodes yet
    Given A registered customer
    And the customer is not allowed to request any more barcodes
    When The customer requests a set of new barcodes
    Then The customer receives an error from the system indicating that he still has barcodes to consume

  Scenario: A non registered customer requests a set of barcodes
    Given A non-registered customer
    When The customer requests a set of new barcodes
    Then The customer receives an error message indicating that he needs to be registered on the system



